﻿using System;
using DigitalCatalogueNetCore.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;

namespace DigitalCatalogueNetCore.Services
{
    public class AuthService
    {

        static HttpClient client = new HttpClient();
        private string indexURL = Startup.Configuration["dbapi_host"];

        public async Task<BiblioUser> Authentication(LoginUserModel loginModel)
        {
            BiblioUser biblioUser = null;
            HttpResponseMessage response = await client.PostAsJsonAsync(indexURL + "/api/User/", loginModel);
            if (response.IsSuccessStatusCode)
            {
                biblioUser = await response.Content.ReadAsAsync<BiblioUser>();
                biblioUser.Token = GetToken(biblioUser.Id, biblioUser.UserStatus);
            }
            return biblioUser;
        }

        private string GetToken(string id, string status)
        {
            string securityKey = "eto_super_dlinniy_klych_kotoriy_po-horoshemy_hranit_ne_tut_a_gde_nibyd_v_faile";
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            var claims = new List<Claim>
                {
                    new Claim("UserId", id),
                    new Claim("UserStatus", status)
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                "user");

            //create token
            var token = new JwtSecurityToken(
                    issuer: "WebAPIServer",
                    audience: "WebAPIClient",
                    expires: DateTime.Now.AddMinutes(15),
                    signingCredentials: signingCredentials,
                    claims: claimsIdentity.Claims
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
